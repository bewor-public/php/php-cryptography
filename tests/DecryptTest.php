<?php

use Bewor\PhpCryptography\Decrypt;
use Bewor\PhpCryptography\Encrypt;
use Yoast\PHPUnitPolyfills\TestCases\TestCase;

class DecryptTest extends TestCase
{
    public function testDecryptPublicKey()
    {
        $publicKey = file_get_contents('tests/certificates-example/public.pem');
        $privateKey = file_get_contents('tests/certificates-example/private.pem');

        $textToEncrypt = 'Hello World!';
        $encrypter = new Encrypt('aes-256-cbc', $publicKey);
        $data = $encrypter->encryptPublicKey($textToEncrypt, false);

        $decrypter = new Decrypt('aes-256-cbc', $privateKey);
        $decryptedText = $decrypter->decryptPrivateKey($data, false);

        $this->assertSame($textToEncrypt, $decryptedText);
    }

    public function testDecryptText()
    {
        $password = Encrypt::randomPassword();
        $publicKey = file_get_contents('tests/certificates-example/public.pem');
        $privateKey = file_get_contents('tests/certificates-example/private.pem');

        $textToEncrypt = 'Hello World!';
        $encrypter = new Encrypt('aes-256-cbc', $publicKey);
        $data = $encrypter->encrypt($textToEncrypt, $password, false);

        $decrypter = new Decrypt('aes-256-cbc', $privateKey);
        $decryptedText = $decrypter->decrypt($data, $password, false);

        $this->assertSame($textToEncrypt, $decryptedText);
    }

    public function testDecryptTextBase64()
    {
        $password = Encrypt::randomPassword();
        $publicKey = file_get_contents('tests/certificates-example/public.pem');
        $privateKey = file_get_contents('tests/certificates-example/private.pem');

        $textToEncrypt = 'Hello World!';
        $encrypter = new Encrypt('aes-256-cbc', $publicKey);
        $data = $encrypter->encrypt($textToEncrypt, $password, false);

        $decrypter = new Decrypt('aes-256-cbc', $privateKey);
        $decryptedText = base64_decode($decrypter->decryptBase64($data, $password, false));

        $this->assertSame($textToEncrypt, $decryptedText);
    }
}
