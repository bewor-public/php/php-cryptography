<?php

use Bewor\PhpCryptography\Encrypt;
use Yoast\PHPUnitPolyfills\TestCases\TestCase;

class EncryptTest extends TestCase
{
    public function testGenerateRandomPassword()
    {
        $passwordA = Encrypt::randomPassword();
        $passwordB = Encrypt::randomPassword();

        $this->assertNotSame($passwordA, $passwordB);
        $this->assertSame(32, strlen($passwordA));
    }

    public function testEncryptPublicKey()
    {
        $password = Encrypt::randomPassword();
        $publicKey = file_get_contents('tests/certificates-example/public.pem');
        $privateKey = file_get_contents('tests/certificates-example/private.pem');

        $encrypter = new Encrypt('aes-256-cbc', $publicKey);
        $encryptedPassword = $encrypter->encryptPublicKey($password, false);

        $data = '';
        $isSucces = \openssl_private_decrypt($encryptedPassword, $data, $privateKey);

        $this->assertTrue($isSucces);
        $this->assertSame($password, $data);
        $this->assertNotSame($password, $encryptedPassword);
    }

    public function testEncryptText()
    {
        $password = Encrypt::randomPassword();
        $publicKey = file_get_contents('tests/certificates-example/public.pem');

        $textToEncrypt = 'Hello World!';
        $encrypter = new Encrypt('aes-256-cbc', $publicKey);
        $data = $encrypter->encrypt($textToEncrypt, $password, false);
        $encryptedPassword = $encrypter->encryptPublicKey($password, false);

        $this->assertNotSame($textToEncrypt, $data);
        $this->assertNotSame($password, $encryptedPassword);
        $this->assertStringStartsWith('Salted__', $data);

        $isBase64 = $this->validateBase64($data);
        $this->assertFalse($isBase64);
    }

    public function testEncryptTextBase64()
    {
        $password = Encrypt::randomPassword();
        $publicKey = \file_get_contents('tests/certificates-example/public.pem');

        $textToEncrypt = 'Hello World!';
        $encrypter = new Encrypt('aes-256-cbc', $publicKey);
        $data = $encrypter->encryptBase64($textToEncrypt, $password, false);
        $encryptedPassword = $encrypter->encryptPublicKey($password, false);

        $this->assertNotSame($textToEncrypt, $data);
        $this->assertNotSame($password, $encryptedPassword);

        $isBase64 = $this->validateBase64($data);
        $this->assertTrue($isBase64);

        $this->assertStringStartsWith('Salted__', \base64_decode($data));
    }

    private function validateBase64(string $data): bool
    {
        return \base64_encode(\base64_decode($data, true)) === $data;
    }
}
