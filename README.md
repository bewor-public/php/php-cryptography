# Bewor Tech php crypto package

## Installation

### Laravel
Require this package in your composer.json and update composer. This will download the package.

    composer require bewor/php-cryptography

## Using

You can create a new Cryptography encrypter instance with any encrypt method allowed on openssl https://php.net/manual/en/function.openssl-encrypt.php.

```php
    use Bewor\PhpCryptography\Encrypt;

    $encrypter = new Encrypt('aes-256-cbc', $publicKey);

    $data = '[data to encrypt]';

    $password = Encrypt::randomPassword();
    $encryptedPassword = $encrypter->encryptPublicKey($password);

    $encrypted = $encrypter->encrypt($data, $password);
    $base64encrypted = $encrypter->encryptBase64($data, $password);
```

or create a new Cryptography decrypter instance with any decrypt method allowed on openssl https://www.php.net/manual/en/function.openssl-decrypt.php.

```php
    use Bewor\PhpCryptography\Decrypt;

    $decrypter = new Decrypt('aes-256-cbc', $privateKey);

    $data = '[data encrypted]';

    ...

    $decryptedPassword = $decrypter->decryptPrivateKey($password);

    $decrypted = $decrypter->decrypt($data, $password);
    $base64decrypted = $decrypter->decryptBase64($data, $password);
```

You can also work with files instead of content.

```php
    use Bewor\PhpCryptography\Encrypt;
    use Bewor\PhpCryptography\Decrypt;

    $encrypter = new Encrypt('aes-256-cbc', $publicKey);
    $decrypter = new Decrypt('aes-256-cbc', $privateKey);

    $filePath = 'path/to/file';

    ...

    $encrypted = $encrypter->encrypt($filePath, $password, true);
    $base64encrypted = $encrypter->encryptBase64($filePath, $password, true);

    $decrypted = $decrypter->decrypt($filePath, $password, true);
    $base64decrypted = $decrypter->decryptBase64($filePath, $password, true);
```
