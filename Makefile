# Executables (local)
DOCKER_COMP = docker compose

# Docker containers
PHP_CONT = $(DOCKER_COMP) exec php

# Executables
PHP      = $(PHP_CONT) php
COMPOSER = $(PHP_CONT) composer
ARTISAN  = $(PHP_CONT) php artisan

# Misc
.DEFAULT_GOAL = help
.PHONY        : help build up start down logs sh composer vendor sf cc

## —— 🎵 🐳 The Docker Makefile 🐳 🎵 ——————————————————————————————————
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9\./_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

## —— Docker 🐳 ————————————————————————————————————————————————————————————————
build-fresh: ## Builds the Docker images fresh (--pull --no-cache)
	@$(DOCKER_COMP) build --pull --no-cache

build: ## Builds the Docker images
	@$(DOCKER_COMP) build --pull

up: ## Start the docker hub in detached mode (no logs)
	@$(DOCKER_COMP) up --detach

start: build up ## Build and start the containers

down: ## Stop the docker hub
	@$(DOCKER_COMP) down --remove-orphans

d: ## Stop the docker hub
	@$(DOCKER_COMP) down --remove-orphans

logs: ## Show live logs
	@$(DOCKER_COMP) logs --tail=0 --follow

sh: ## Connect to the PHP FPM container
	@$(PHP_CONT) sh

## —— Composer 🧙 ——————————————————————————————————————————————————————————————
composer: ## Run composer, pass the parameter "c=" to run a given command, example: make composer c='req symfony/orm-pack'
	@$(eval c ?=)
	@$(COMPOSER) $(c)

vendor: ## Install vendors according to the current composer.lock file
vendor: c=install --prefer-dist --no-dev --no-progress --no-scripts --no-interaction
vendor: composer

install: ## Install vendors according to the current composer.lock file
install: c=install --prefer-dist --no-progress --no-scripts --no-interaction
install: composer

## —— Artisan 🎵 ———————————————————————————————————————————————————————————————
artisan: ## List all Artisan commands or pass the parameter "c=" to run a given command, example: make sf c=about
	@$(eval c ?=)
	@$(ARTISAN) $(c)

tinker: c=tinker ## Tinker terminal
tinker: artisan

cc: c=o:c ## Clear the cache
cc: artisan

rl: c=r:l ## route list
rl: artisan

fresh: c=migrate:fresh ## migrate fresh database
fresh: artisan

migrate: c=migrate ## migrate fresh database
migrate: artisan

autoload: c=dumpautoload ## dump autoload
autoload: composer

encrypt: c=bewor:encrypt
encrypt: artisan

ac1: c=bewor:ac-sync-up
ac1: artisan

ac2: c=bewor:ac-sync-tag-not-videoid-finished
ac2: artisan

ac3: c=bewor:ac-sync-tag-not-certificate-issued
ac3: artisan


fichaje: c=quakki:fichaje
fichaje: artisan



## —— Test ————————————————————————————————————————————————————————————

test: ## Run the tests
	@$(PHP) ./vendor/bin/phpunit

## —— Code style ✨ ————————————————————————————————————————————————————————————

psr: ## code style check
	@$(PHP) ./vendor/bin/phpcs

fix: ## code style fix
	@$(PHP) ./vendor/bin/phpcbf
