<?php

declare(strict_types=1);

namespace Bewor\PhpCryptography;

use Bewor\PhpCryptography\Core;

final class Decrypt
{
    public function __construct(private string $cipherAlgorithm, private string $privateKey)
    {
    }

    public function decryptPrivateKey(string $data, bool $base64 = true): string
    {
        return Core::privateDecrypt($data, $this->privateKey, $base64);
    }

    public function decrypt(string $file, string $password, bool $isFile = false): string
    {
        return Core::decrypt($file, $password, $this->cipherAlgorithm, $isFile);
    }

    public function decryptBase64(string $file, string $password, bool $isFile = false): string
    {
        $data = $this->decrypt($file, $password, $isFile);
        return \base64_encode($data);
    }
}
