<?php

declare(strict_types=1);

namespace Bewor\PhpCryptography;

use Bewor\PhpCryptography\Core;

final class Encrypt
{
    public function __construct(private string $cipherAlgorithm, private string $publicKey)
    {
    }

    public function encrypt(string $file, string $password, bool $isFile = false): string
    {
        return Core::encrypt($file, $password, $this->cipherAlgorithm, $isFile);
    }

    public function encryptBase64(string $file, string $password, bool $isFile = false): string
    {
        $data = $this->encrypt($file, $password, $isFile);
        return \base64_encode($data);
    }

    public function encryptPublicKey(string $data, bool $base64 = true): string
    {
        return Core::publicEncrypt($data, $this->publicKey, $base64);
    }

    public function opensslCommand(string $path, string $password): string
    {
        $exec = 'openssl enc -' . $this->cipherAlgorithm . ' -d -in ' . $path . ' -pbkdf2 -pass pass:' . $password;
        return $exec;
    }

    public static function randomPassword(): string
    {
        return bin2hex(Core::rand(16));
    }

    public static function hashContent(string $content): string
    {
        return md5($content);
    }
}
