<?php

declare(strict_types=1);

namespace Bewor\PhpCryptography;

final class Core
{
    public const SALT_SIZE = 8;
    public const ENCRYPT_KEY_LENGHT = 32;
    public const PBKD_ENCRYPT_KEY_LENGHT = 48;
    public const PBKD_ITERATIONS = 10000;
    public const PBKD_DIGEST = 'sha256';

    public static function encrypt(string $fileOrText, string $password, string $cipherAlgorithm, bool $isFile = false): string
    {
        $salt = self::rand(self::SALT_SIZE);
        $pbkdPassword = openssl_pbkdf2($password, $salt, self::PBKD_ENCRYPT_KEY_LENGHT, self::PBKD_ITERATIONS, self::PBKD_DIGEST);

        $ivLenght = openssl_cipher_iv_length($cipherAlgorithm);
        $saltPrefix = "Salted__" . $salt;
        $iv = substr($pbkdPassword, self::ENCRYPT_KEY_LENGHT, $ivLenght);

        $plaintext = $fileOrText;
        if ($isFile) {
            $plaintext = self::getFile($fileOrText);
        }

        $result = \openssl_encrypt(
            data: $plaintext,
            cipher_algo: $cipherAlgorithm,
            passphrase: $pbkdPassword,
            options: OPENSSL_RAW_DATA,
            iv: $iv
        );

        if ($result === false) {
            throw new \Exception('Error encrypting data');
        }

        return $saltPrefix . $result;
    }

    public static function decrypt(string $fileOrText, string $password, string $cipherAlgorithm, bool $isFile = false): string
    {
        $plaintext = $fileOrText;
        if ($isFile) {
            $plaintext = self::getFile($fileOrText);
        }

        $salt = substr($plaintext, core::SALT_SIZE, core::SALT_SIZE);
        $pbkdPassword = openssl_pbkdf2($password, $salt, core::PBKD_ENCRYPT_KEY_LENGHT, core::PBKD_ITERATIONS, core::PBKD_DIGEST);
        $ivLenght = openssl_cipher_iv_length($cipherAlgorithm);
        $saltPrefix = "Salted__" . $salt;
        $iv = substr($pbkdPassword, Core::ENCRYPT_KEY_LENGHT, $ivLenght);

        $plaintext = str_replace($saltPrefix, '', $plaintext);

        $result = \openssl_decrypt(
            data: $plaintext,
            cipher_algo: $cipherAlgorithm,
            passphrase: $pbkdPassword,
            options: OPENSSL_RAW_DATA,
            iv: $iv
        );

        if ($result === false) {
            throw new \Exception('Error decrypting data');
        }

        return $result;
    }

    public static function publicEncrypt(
        string $data,
        \OpenSSLAsymmetricKey|\OpenSSLCertificate|array|string $publicKey,
        bool $base64 = true,
    ): string {
        $encrypted = '';
        $result = openssl_public_encrypt($data, $encrypted, $publicKey);

        if ($result === false) {
            throw new \Exception('Error encrypting data');
        }

        if ($base64) {
            return base64_encode($encrypted);
        }

        return $encrypted;
    }

    public static function privateDecrypt(
        string $data,
        \OpenSSLAsymmetricKey|\OpenSSLCertificate|array|string $privateKey,
        bool $base64 = true
    ): string {
        if ($base64) {
            $data = base64_decode($data);
        }
        $decrypted = '';
        $result = openssl_private_decrypt($data, $decrypted, $privateKey);
        if (!$result) {
            throw new \Exception('Error decrypting data');
        }
        return $decrypted;
    }

    public static function strToHex(string $x): string
    {
        return bin2hex($x);
    }

    public static function getFile(string $file): string
    {
        if (!\file_exists($file)) {
            throw new \Exception('File not found');
        }

        $content = \file_get_contents($file);

        if ($content === false) {
            throw new \Exception('Error reading file');
        }

        return $content;
    }

    public static function rand(int $size): string
    {
        return \random_bytes($size);
    }
}
